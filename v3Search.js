const hubspot = require('@hubspot/api-client');
require('dotenv').config()
//
const {First, The_best, Mango} = require("./companyIds")
const request = require("request");
const axios = require("axios")

//
// async function getDeals1(companyId) {
//     let getCompanyById = `https://api.hubapi.com/crm/v3/objects/companies/${companyId}?hapikey=${process.env.API_KEY}&associations=deal`
//     let companyData = await axios.get(getCompanyById)
//     companyData = {...companyData.data};
//
//     let promises = companyData.associations.deals.results.map(({id}) => {
//         let getDeal = `https://api.hubapi.com/crm/v3/objects/deals/${id}?hapikey=${process.env.API_KEY}`
//         return axios.get(getDeal);
//     })
//     let res = await Promise.all(promises);
//     let dealsInfo = res.map(item => item.data)
//     // console.log(dealsInfo)
//     console.log(companyData.associations.deals.results)
//     // console.log({companyData,dealsInfo})
// }
// getDeals1(Mango.id)

async function getData(){
    const hubspotClient = new hubspot.Client({apiKey: process.env.API_KEY});
    const limit = 10;
    const after = undefined;
    const properties = undefined;
    const propertiesWithHistory = undefined;
    const associations =[ "company"];
    const archived = false;

    try {
        const apiResponse = await hubspotClient.crm.deals.basicApi.getPage(limit, after, properties, propertiesWithHistory, associations, archived);
        // console.log(apiResponse)
        apiResponse.results.forEach(item=> {
            console.log(item)
        })
        // console.log(JSON.stringify(apiResponse.body, null, 2));
    } catch (e) {
        e.message === 'HTTP request failed'
            ? console.error(JSON.stringify(e.response, null, 2))
            : console.error(e)
    }

}

//

getData()
