
var request = require("request")
require('dotenv').config()

const count = 5;

function getDeals(offset) {

    const hapikeyParam = process.env.API_KEY;
    const paramsString = `?count=${count}&${hapikeyParam}&includeAssociations=true&properties=dealName`;
const returnedDeals = [];
    // crm/v3/objects/deals?hapikey=eu1-d3e4-2ef4-4d52-b964-c9e2210229c0&associations=company
    const finalUrl = `https://api.hubapi.com/deals/v1/deal/associated/company/5875208181/paged?hapikey=eu1-d3e4-2ef4-4d52-b964-c9e2210229c0&includeAssociations=true&limit=10&properties=dealname`
    request(finalUrl, (error, response, body) => {
        if (error) {
            console.log('error', error)
            throw new Error
        }
        const parsedBody = JSON.parse(body)

        parsedBody.deals.forEach(deal => {
            console.log(deal)
            returnedDeals.push(deal)
        });
        if (parsedBody['hasMore']) {
            getDeals(parsedBody['offset'])
        } else {
            //print out all deals
            console.log(returnedDeals)
        }
    })
};

getDeals()
